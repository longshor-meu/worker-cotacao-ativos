package br.com.leglaucio.longshort.workercotacaoativos.template;

import java.time.LocalDate;

import br.com.leglaucio.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class HistoricoCotacaoTemplateLoader implements TemplateLoader{


	public static final String PETR4 = "PETR4";
	
	@Override
	public void load() {
		Fixture.of(HistoricoCotacao.class).addTemplate(PETR4, new Rule() {
			{
				add("codigoAtivo", "PETR4");
				add("valorCotacao", 10D); 
				add("data", LocalDate.now());
				//hora nao vai colocar porque é pra criar um historico diario
			}
		});
		
	}

}
