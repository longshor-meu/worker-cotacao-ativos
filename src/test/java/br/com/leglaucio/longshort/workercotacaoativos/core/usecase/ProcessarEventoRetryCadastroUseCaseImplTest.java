package br.com.leglaucio.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.com.leglaucio.longshort.base.dto.response.ResponseDataErro;
import br.com.leglaucio.longshort.base.dto.response.TipoDeErroEnum;
import br.com.leglaucio.longshort.base.gateway.ISalvarGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemDeadLetterGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemRetryGateway;
import br.com.leglaucio.longshort.workercotacaoativos.template.AtivoRequestTemplateLoader;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;

@RunWith(MockitoJUnitRunner.class)
public class ProcessarEventoRetryCadastroUseCaseImplTest {

	@InjectMocks
	private ProcessarEventoRetryCadastroUseCaseImpl usecase;

	@Mock
	private ISalvarGateway<HistoricoCotacao> salvarGateway;
	
	@Mock
	private IBuscarCotacoesGateway buscarCotacoesGateway;

	@Mock
	protected IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway;
	
	@Mock
	private IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway;
	

	@Mock
	private IConfiguracaoGateway configuracaoGateway;
	
	@BeforeClass
	public static void beforeClass() {
		FixtureFactoryLoader.loadTemplates("br.com.leglaucio.longshort.workercotacaoativos.template");
	}

	@Before
	public void before() {
		Mockito.when(configuracaoGateway.getQuantidadeDiasParaCotacaoNoCadastroAtivo()).thenReturn(300);
	}
	
	@Test
	public void deve_salvar_carga_cotacao_na_base() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		List<HistoricoCotacao> historicos = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			historicos.add(new HistoricoCotacao());
		}
		
		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		response.setHistoricos(historicos);
		
		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
		.thenReturn(null);
		
		Mockito.when(buscarCotacoesGateway.buscarCotacoes(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(), Mockito.eq(false)))
		.thenReturn(response);
		
		usecase.executar(request);
		
		Mockito.verify(buscarCotacoesGateway).buscarCotacoes(request.getCodigo(), LocalDate.now().minus(300, ChronoUnit.DAYS), 
				LocalDate.now(), false);
		Mockito.verify(salvarGateway, Mockito.times(100)).salvar(Mockito.any());
		
	}
	
	@Test
	public void nao_deve_salvar_com_campos_ausentes() {
		AtivoRequest request = new AtivoRequest();
		usecase.executar(request);
		
		Mockito.verify(gerarMensagemDeadLetterGateway).gerar(Mockito.any(AtivoDeadLetterResponse.class));
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}
	
	@Test
	public void nao_deve_salvar_carga_ja_realizada() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
		.thenReturn(new HistoricoCotacao());
		
		usecase.executar(request);
		
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
	}
	
	@Test
	public void deve_gerar_deadletter_quando_timeout() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.TIMEOUT, "Teste unitario de timeout"));
		
		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
		.thenReturn(null);
		
		Mockito.when(buscarCotacoesGateway.buscarCotacoes(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(), Mockito.eq(false)))
		.thenReturn(response);
		
		usecase.executar(request);
		
		Mockito.verify(buscarCotacoesGateway, Mockito.times(3)).buscarCotacoes(request.getCodigo(), LocalDate.now().minus(300, ChronoUnit.DAYS), 
				LocalDate.now(), false);
		Mockito.verify(gerarMensagemDeadLetterGateway).gerar(Mockito.any(AtivoDeadLetterResponse.class));
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
		
	}
	
	@Test
	public void deve_gerar_deadletter_quando_houver_erro_que_nao_timeout() {
		AtivoRequest request = Fixture.from(AtivoRequest.class).gimme(AtivoRequestTemplateLoader.PETR4);
		
		HistoricoCotacaoResponse response = new HistoricoCotacaoResponse();
		response.getResponseData().adicionarErro(new ResponseDataErro(TipoDeErroEnum.NAO_ENCONTRADO, "Teste unitario de entidade nao encontrada"));
		
		Mockito.when(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))
		.thenReturn(null);
		
		Mockito.when(buscarCotacoesGateway.buscarCotacoes(Mockito.eq(request.getCodigo()), Mockito.any(), Mockito.any(), Mockito.eq(false)))
		.thenReturn(response);
		
		usecase.executar(request);
		
		Mockito.verify(buscarCotacoesGateway).buscarCotacoes(request.getCodigo(), LocalDate.now().minus(300, ChronoUnit.DAYS), 
				LocalDate.now(), false);
		Mockito.verify(gerarMensagemDeadLetterGateway).gerar(Mockito.any(AtivoDeadLetterResponse.class));
		Mockito.verify(salvarGateway, Mockito.times(0)).salvar(Mockito.any());
		
	}
}
