package br.com.leglaucio.longshort.workercotacaoativos.template;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class AtivoRequestTemplateLoader implements TemplateLoader {

	public static final String RANDOM = "RANDOM";
	public static final String PETR4 = "PETR4";
	public static final String VALE5 = "VALE5";
	
	@Override
	public void load() {
		Fixture.of(AtivoRequest.class).addTemplate(RANDOM, new Rule() {
			{
				add("codigo", firstName());
				add("descricao", name());
			}
		});
		
		Fixture.of(AtivoRequest.class).addTemplate(PETR4, new Rule() {
			{
				add("codigo", "PETR4");
				add("descricao", name());
			}
		});
		
		Fixture.of(AtivoRequest.class).addTemplate(VALE5, new Rule() {
			{
				add("codigo", "VALE5");
				add("descricao", name());
			}
		});
	}
	
}