package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway;

public interface IBuscarCotacoesSalvasPorCodigoAtivoGateway {

	Object buscarPrimeiraCotacao(String codigo);

	
}
