package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

public interface IGerarMensagemRetryGateway {
	/**
	 * Gera a mensagem de retry
	 * @param response
	 * */
	void gerar(AtivoRequest request);

}
