package br.com.leglaucio.longshort.workercotacaoativos.infra.dataprovider.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.http.MediaType;

/**
 * mapeia a api do  https://www.alphavantage.co/
 * 
 * 
 * 
{
    "Meta Data": {
        "1. Information": "Daily Prices (open, high, low, close) and Volumes",
        "2. Symbol": "PETR4.SA",
        "3. Last Refreshed": "2019-10-30",
        "4. Output Size": "Compact",
        "5. Time Zone": "US/Eastern"
    },
    "Time Series (Daily)": {
        "2019-10-30": {
            "1. open": "29.7700",
            "2. high": "30.0900",
            "3. low": "29.4700",
            "4. close": "30.0800",
            "5. volume": "63001200"
        },
 * 
 * 
 * @author glaucio
 *
 */

@FeignClient(url = "https://www.alphavantage.co/", name = "alphavantageApi")  //esse name aqui nao precisa bater com nada vc inventa um
public interface AlphaVantageApi {

		@GetMapping(value = "query", produces = MediaType.APPLICATION_JSON_VALUE)
		void buscarCotacoes(@RequestParam("funcao") String funcao, @RequestParam("simbolo") String simbolo, @RequestParam("apikey") String apikey);
	
}
