package br.com.leglaucio.longshort.workercotacaoativos.infra.entrypoint.kafka;


import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.IProcessarEventoRetryCadastroUseCase;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics="${app.kafka.cadastroativo.retry.topic.name}")
public class ConsumerRetryKafka {

	private final IProcessarEventoRetryCadastroUseCase usecase;
	private static final Integer TEMPO_RETRY_SEGUNDOS = 5;
	
	public ConsumerRetryKafka(IProcessarEventoRetryCadastroUseCase usecase) {
		super();
		this.usecase = usecase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment, @Header(KafkaHeaders.RECEIVED_TIMESTAMP) Long timestamp) throws InterruptedException {
		
		//TODO: Essa regra de tempo de retry deveria estar num use case
		Integer segundos = LocalDateTime.now().minus(timestamp, ChronoUnit.MILLIS).getSecond(); 
		if (segundos >= TEMPO_RETRY_SEGUNDOS) {
			usecase.executar(request);
			acknowledgment.acknowledge();
		} else {
			Thread.sleep((TEMPO_RETRY_SEGUNDOS-segundos)*1000L); 
		}
		//nao precisa fazer NACK aqui, o spring faz isso sozinho ja que nao fez o ack
	}
}
