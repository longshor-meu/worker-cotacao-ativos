package br.com.leglaucio.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemRetryGateway;

@Component
@Profile("prod") //pra nao dar erro nso testes unitarios que injeta o Fake dessa classe
public class ProducerRetry implements IGerarMensagemRetryGateway {

	@Value(value="${app.kafka.cadastroativo.retry.topic.name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String, AtivoRequest> kafkaTemplate;

	public ProducerRetry(KafkaTemplate<String, AtivoRequest> kafkaTemplate) {
		super();
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoRequest request) {
		kafkaTemplate.send(kafkaTopicName, request);
		
	}


}
