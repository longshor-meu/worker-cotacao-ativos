package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto;

import java.util.ArrayList;
import java.util.List;

import br.com.leglaucio.longshort.base.dto.response.BaseResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.entity.HistoricoCotacao;

public class HistoricoCotacaoResponse extends BaseResponse {

	private List<HistoricoCotacao> historicos = new ArrayList<>();

	public List<HistoricoCotacao> getHistoricos() {
		return historicos;
	}

	public void setHistoricos(List<HistoricoCotacao> historicos) {
		this.historicos = historicos;
	}
}
