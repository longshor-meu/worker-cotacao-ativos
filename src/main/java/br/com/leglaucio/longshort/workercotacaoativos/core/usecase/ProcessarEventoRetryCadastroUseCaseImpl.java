package br.com.leglaucio.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.leglaucio.longshort.base.dto.response.TipoDeErroEnum;
import br.com.leglaucio.longshort.base.gateway.ISalvarGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemDeadLetterGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemRetryGateway;

public class ProcessarEventoRetryCadastroUseCaseImpl extends ProcessarEventoCadastroUseCaseImpl implements IProcessarEventoRetryCadastroUseCase {

	private IBuscarCotacoesGateway buscarCotacoesGateway;
	private IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway;
	protected IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway;
	protected IConfiguracaoGateway configuracaoGateway;
	
	
	public ProcessarEventoRetryCadastroUseCaseImpl(ISalvarGateway<HistoricoCotacao> salvarGateway,
			IBuscarCotacoesGateway buscarCotacoesGateway,
			IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway,
			IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway,
			IConfiguracaoGateway configuracaoGateway, IGerarMensagemRetryGateway gerarMensagemRetryGateway,
			IBuscarCotacoesGateway buscarCotacoesGateway2,
			IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway2,
			IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway2,
			IConfiguracaoGateway configuracaoGateway2) {
		super(salvarGateway, buscarCotacoesGateway, gerarMensagemDeadLetterGateway,
				buscarCotacoesSalvasPorCodigoAtivoGateway, configuracaoGateway, gerarMensagemRetryGateway);
		buscarCotacoesGateway = buscarCotacoesGateway2;
		gerarMensagemDeadLetterGateway = gerarMensagemDeadLetterGateway2;
		buscarCotacoesSalvasPorCodigoAtivoGateway = buscarCotacoesSalvasPorCodigoAtivoGateway2;
		configuracaoGateway = configuracaoGateway2;
	}



	@Override
	protected HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, LocalDate inicio, LocalDate fim,
			int tentativas) {
		HistoricoCotacaoResponse responseCotacao = buscarCotacoesGateway.buscarCotacoes(request.getCodigo(), inicio, fim,
				false);
		if (!responseCotacao.getResponseData().getErros().isEmpty()) {

			if (responseCotacao.getResponseData().getErros().stream().filter(e -> TipoDeErroEnum.TIMEOUT.equals(e.getCodigoErro()))
					.count() > 0) {
				if (tentativas < 2) {
					return buscarCotacoes(request, inicio, fim, ++tentativas);
				} else {
					log.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
					gerarMensagemDeadLetterGateway
							.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
				}
			} else {
				log.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
				gerarMensagemDeadLetterGateway
						.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
			}
		}

		return responseCotacao;
	}


}
