package br.com.leglaucio.longshort.workercotacaoativos.core.usecase;

import br.com.leglaucio.longshort.base.usecae.IUseCase;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Processa o evento de cadastro de novo ativo buscando todas as cotacoes desse ativo
 * @author glaucio
 *
 */
public interface IProcessarEventoCadastroUseCase extends IUseCase<AtivoRequest, Void> {

}
