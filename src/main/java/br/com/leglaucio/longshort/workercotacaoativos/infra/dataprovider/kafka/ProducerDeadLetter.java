package br.com.leglaucio.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemDeadLetterGateway;

@Component
@Profile("prod") //pra nao dar erro nso testes unitarios que injeta o Fake dessa classe
public class ProducerDeadLetter implements IGerarMensagemDeadLetterGateway {

	@Value(value="${app.kafka.cadastroativo.deadletter.topic.name}")
	private String kafkaTopicName;
	
	private final KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate;

	public ProducerDeadLetter(KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate) {
		super();
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoDeadLetterResponse response) {
		kafkaTemplate.send(kafkaTopicName, response);
		
	}
	
}
