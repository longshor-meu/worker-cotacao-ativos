package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;

public interface IGerarMensagemDeadLetterGateway {
	
	/**
	 * Gera a mensagem
	 * @param response
	 */
	void gerar(AtivoDeadLetterResponse response);

}
