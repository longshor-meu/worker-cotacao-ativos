package br.com.leglaucio.longshort.workercotacaoativos.infra.entrypoint.kafka;


import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.IProcessarEventoCadastroUseCase;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics="${app.kafka.cadastroativo.topic.name}")
public class ConsumerKafka {

	private final IProcessarEventoCadastroUseCase usecase;
	
	public ConsumerKafka(IProcessarEventoCadastroUseCase usecase) {
		super();
		this.usecase = usecase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment) {
		usecase.executar(request);
		acknowledgment.acknowledge();
	}
}
