package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway;

import java.time.LocalDate;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;

/**
 * Busca cotacoes de um determinado ativo
 * @author glaucio
 *
 */
public interface  IBuscarCotacoesGateway {

	/**
	 * Realiza busca de cotacoesd o ativo informado entre as datas de incio e fim informado
	 * se intraday for true retorna a cotacao hora a hora
	 * se intradau for false retorna a cotacao de fechamento de cada dia
	 * @param codigoAtivo
	 * @param incio
	 * @param fim
	 * @param intraday
	 */
	HistoricoCotacaoResponse buscarCotacoes(String codigoAtivo, LocalDate incio, LocalDate fim, boolean intraday);
	
}
