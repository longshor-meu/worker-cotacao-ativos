package br.com.leglaucio.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.leglaucio.longshort.base.dto.response.ResponseDataErro;
import br.com.leglaucio.longshort.base.dto.response.TipoDeErroEnum;
import br.com.leglaucio.longshort.base.gateway.ISalvarGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IBuscarCotacoesSalvasPorCodigoAtivoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IConfiguracaoGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemDeadLetterGateway;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IGerarMensagemRetryGateway;

public class ProcessarEventoCadastroUseCaseImpl implements IProcessarEventoCadastroUseCase {

	private ISalvarGateway<HistoricoCotacao> salvarGateway;
	private IBuscarCotacoesGateway buscarCotacoesGateway;
	private IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway;
	protected final IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway;
	protected final IConfiguracaoGateway configuracaoGateway;
	private final IGerarMensagemRetryGateway gerarMensagemRetryGateway;
	
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	public ProcessarEventoCadastroUseCaseImpl(ISalvarGateway<HistoricoCotacao> salvarGateway,
			IBuscarCotacoesGateway buscarCotacoesGateway,
			IGerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway,
			IBuscarCotacoesSalvasPorCodigoAtivoGateway buscarCotacoesSalvasPorCodigoAtivoGateway,
			IConfiguracaoGateway configuracaoGateway,
			IGerarMensagemRetryGateway gerarMensagemRetryGateway) {
		super();
		this.salvarGateway = salvarGateway;
		this.buscarCotacoesGateway = buscarCotacoesGateway;
		this.gerarMensagemDeadLetterGateway = gerarMensagemDeadLetterGateway;
		this.buscarCotacoesSalvasPorCodigoAtivoGateway = buscarCotacoesSalvasPorCodigoAtivoGateway;
		this.configuracaoGateway = configuracaoGateway;
		this.gerarMensagemRetryGateway = gerarMensagemRetryGateway;
	}

	@Override
	public Void executar(AtivoRequest request) {
		log.info("Processando evento para carregar historico de cotacoes do ativo " + request.getCodigo());
		AtivoDeadLetterResponse response = new AtivoDeadLetterResponse();
		validarCamposObrigatorios(request, response);
		if (!response.getResponseData().getErros().isEmpty()) {
			log.warn("Gerando mensagem de deadletter");
			gerarMensagemDeadLetterGateway.gerar(response);
			return null;
		}
		if (Objects.nonNull(buscarCotacoesSalvasPorCodigoAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))) {
			log.warn("Ignorando mensagem duplicada para ativo {}", request.getCodigo());
			return null;
		}

		LocalDate dataFinal = LocalDate.now();
		LocalDate dataInicial = dataFinal.minusDays(configuracaoGateway.getQuantidadeDiasParaCotacaoNoCadastroAtivo());
		log.info("Iniciando busca das cotações no período de {} até {}", dataInicial, dataFinal);
		HistoricoCotacaoResponse responseCotacao = buscarCotacoes(request, dataInicial, dataFinal, 0);

		if (responseCotacao.getResponseData().getErros().isEmpty()) {
			responseCotacao.getHistoricos().forEach(salvarGateway::salvar);
		}

		return null;
	}
	
	//
	// Professor implementou todos os outros metodos dessa classe, exceto o metodo abaixo, em uma classe base
	//
	protected HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, LocalDate inicio, LocalDate fim, int tentativas) {
		HistoricoCotacaoResponse responseCotacao = buscarCotacoesGateway.buscarCotacoes(request.getCodigo(), inicio, fim,
				false);
		if (!responseCotacao.getResponseData().getErros().isEmpty()) {

			if (responseCotacao.getResponseData().getErros().stream().filter(e -> TipoDeErroEnum.TIMEOUT.equals(e.getCodigoErro())).count() > 0) {
				if (tentativas < 2) {
					return buscarCotacoes(request, inicio, fim, ++tentativas);
				} else {
					log.info("Gerando mensagem de Retry do ativo {}", request.getCodigo());
					gerarMensagemRetryGateway.gerar(request);
				}
			} else {
				log.info("Gerando mensagem de DeadLetter do ativo {}", request.getCodigo());
				gerarMensagemDeadLetterGateway
						.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
			}
		}

		return responseCotacao;
	}

	private void validarCamposObrigatorios(AtivoRequest request, AtivoDeadLetterResponse response) {
		if (StringUtils.isBlank(request.getCodigo())) {
			response.getResponseData()
					.adicionarErro(new ResponseDataErro(TipoDeErroEnum.CAMPOS_OBRIGATORIOS, "Codigo é obrigatorio"));
			log.error("Campo Codigo é obrigatorio");
		}
	}
}
