package br.com.leglaucio.longshort.workercotacaoativos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@EnableKafka
public class WorkerCotacaoAtivosApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkerCotacaoAtivosApplication.class, args);
	}

}
