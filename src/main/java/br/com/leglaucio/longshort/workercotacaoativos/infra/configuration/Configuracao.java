package br.com.leglaucio.longshort.workercotacaoativos.infra.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway.IConfiguracaoGateway;

@Configuration
public class Configuracao implements IConfiguracaoGateway{

	@Value(value="${app.QuantidadeDiasParaCotacaoNoCadastroAtivo}")
	private Integer qtdDias;

	@Override
	public Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo() {
		return qtdDias;
	}

}
