package br.com.leglaucio.longshort.workercotacaoativos.core.usecase.gateway;

public interface IConfiguracaoGateway {
	
	public Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo();

}
