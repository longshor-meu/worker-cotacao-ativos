package br.com.leglaucio.longshort.workercotacaoativos.core.usecase;

import br.com.leglaucio.longshort.base.usecae.IUseCase;
import br.com.leglaucio.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

/**
 * Processa o evento de retray de cadastro de ativo
 * @author glaucio
 *
 */
public interface IProcessarEventoRetryCadastroUseCase extends IUseCase<AtivoRequest, Void>{

}
