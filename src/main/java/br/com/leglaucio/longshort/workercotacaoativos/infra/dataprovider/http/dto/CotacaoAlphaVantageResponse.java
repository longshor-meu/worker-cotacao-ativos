package br.com.leglaucio.longshort.workercotacaoativos.infra.dataprovider.http.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CotacaoAlphaVantageResponse {

	@JsonProperty(value = "Meta Data")
	private CotacaoAlphaVantageMetadata metadata;
	
	@JsonProperty(value = "Time Series (Daily)")
	private Map<String, CotacaoAlphaVantageData> timeseries;
	
	public CotacaoAlphaVantageMetadata getMetadata() {
		return metadata;
	}
	public void setMetadata(CotacaoAlphaVantageMetadata metadata) {
		this.metadata = metadata;
	}
	public Map<String, CotacaoAlphaVantageData> getTimeseries() {
		return timeseries;
	}
	public void setTimeseries(Map<String, CotacaoAlphaVantageData> timeseries) {
		this.timeseries = timeseries;
	}

	
	
}
