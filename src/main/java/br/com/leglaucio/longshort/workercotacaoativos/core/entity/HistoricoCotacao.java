package br.com.leglaucio.longshort.workercotacaoativos.core.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.leglaucio.longshort.base.entity.BaseEntity;

public class HistoricoCotacao extends BaseEntity {

	private String codigoAtivo;
	private Double valorCotacao;
	private LocalDate data;
	private LocalTime hora;
	
	//private BigDecimal PUcomMuitasCasas; //usado para numeros grandes com bastante precisao nos decinais
	
	
	public String getCodigoAtivo() {
		return codigoAtivo;
	}

	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}

	public Double getValorCotacao() {
		return valorCotacao;
	}

	public void setValorCotacao(Double valorCotacao) {
		this.valorCotacao = valorCotacao;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

}
